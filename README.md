# OpenCV_FaceRecognition

# Processament de les dades

El que fa el programa primerament, es crear una finestra que conte una Trackbar per a controlar el nivell de gaussian
blur que s'aplicarà sobre la cara.

Posteriorment en un array es posen tots els noms de les imatges que tenim de persones que volem que reconeixi
per exemple nosaltres en tenim 4, a més a més se li posa un altre camp que és el nom de la persona per tal de
mostrar-lo una vegada reconeguda la persona.

Una vegada introduides les dades es guarden en dos arrays diferents un que correspona les imatges i l'altre als
noms de les persones, es guarden en el mateix index per tal de poder consultarles després i que coincideixin.

El scale_rate està posat a 0.75 ja que contra més baix està més rapid funciona però és menys precís, i 1, es bastant
lent però molt precís. Nosaltres l'hem deixat en 0.75 ja que es el valor més equilibrat que hem trovat entre rendiment
i precisió.


# Explicació del programa en execució


Una vegada el programa està en execució amb la càmera, el que fa es dividir la imatge x 0.75, així la precisió es bastant
bona amb un rendiment acceptable, aquesta imatge es processa i el que fa es buscar cares en la imatge, una vegada troba les
cares, les compara amb l'array de cares que em setejat en un principi. Si la cara coincideix, agafa el nom que li correspon,
pero en cas que no coincideixi amb ninguna cara, se li assigna el nom de unknown "Desconegut".

Posteriorment, de les face_locations que retorna la llibrería, es busquen els quatre punts que formen el rectangle, i es
dibuixa amb opencv, amb el marge d'abaix "filled", en aquest marge reomplert, es posa el nom de la cara, o bé el nom de la 
persona si ha fet match amb alguna coincideincia de l'array de persones o "unknown", si no el coneix.

Després d'aixo si es una persona desconeguda, em aplicat una censura, que s'aplica dins del rectangle de la cara sense agafar
els marges, ni el marge on hi ha el nom, i s'assigna un GaussianBlur, per a donar un efecte més natural.

![Exemple de blur](img/capturablur.png?raw=true "Exemple de blur")

Aquest blur pot ser modificat per l'usuari, ja que amb un trackbar que hi ha a la part inferior de la finestra es pot assignar,
un valor més gran o més petit, inclús eliminar-lo.