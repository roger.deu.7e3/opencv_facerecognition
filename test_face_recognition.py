import face_recognition
import cv2
import numpy as np

def nothing(x):
    pass


video_capture = cv2.VideoCapture(0)
cv2.namedWindow('Video')
cv2.createTrackbar('gaussian kernel blur (2n -1)', 'Video', 25, 50, nothing)

persons = []
persons.append(("obama.jpg", "Barack Obama"))
persons.append(("biden.jpg", "Biden Joe"))
persons.append(("roger.jpg", "Roger Deu"))
#persons.append(("daidis.png", "Daidis"))


# Create arrays of known face encodings and their names
known_face_encodings = []
known_face_names = []

for p in persons:
    image = face_recognition.load_image_file(f"img/{p[0]}")
    known_face_encodings.append(face_recognition.face_encodings(image)[0])
    known_face_names.append(p[1])

# Initialize some variables
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

rescale_rate = 0.75

while True:
    # Grab a single frame of video
    ret, frame = video_capture.read()

    # Resize frame of video to 1/4 size for faster face recognition processing
    small_frame = cv2.resize(frame, (0, 0), fx=rescale_rate, fy=rescale_rate)

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_small_frame = small_frame[:, :, ::-1]

    # Only process every other frame of video to save time
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
            name = "Unknown"

            # # If a match was found in known_face_encodings, just use the first one.
            # if True in matches:
            #     first_match_index = matches.index(True)
            #     name = known_face_names[first_match_index]

            # Or instead, use the known face with the smallest distance to the new face
            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]

            face_names.append(name)

    process_this_frame = not process_this_frame

    # Display the results
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        top = int(top / rescale_rate) + 10
        right = int(right / rescale_rate) + 10
        bottom = int(bottom / rescale_rate) + 10
        left = int(left / rescale_rate) + 10

        # Draw a box around the face
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # Draw a label with a name below the face
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        # Display the resulting image
        #     cara = frame[top:right, bottom:left]
        #     cara = cv2.blur(cara, (2, 2))
        #     frame[top:right, bottom:left] = cara

        if name == "Unknown":
            blur = cv2.getTrackbarPos('gaussian kernel blur (2n -1)', 'Video')
            blur = 2 * blur + 1
            if left > frame.shape[0] / 2:
                frame[top:bottom - 35, left:right] = cv2.GaussianBlur(frame[top:bottom - 35, left:right], (blur, blur), 0)
            else:
                frame[top:bottom - 35, left:right] = cv2.GaussianBlur(frame[top:bottom - 35, left:right], (blur, blur), 0)

    # frame = cv2.blur(frame, (20, 20))
    cv2.imshow('Video', frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release handle to the webcam
video_capture.release()
cv2.destroyAllWindows()
